# VATSIM Airspace OpenAir

Small Utility to create a OpenAir file with VATSIM airspaces based on the
[VATSpy Data Project](https://github.com/vatsimnetwork/vatspy-data-project).
The resulting file can be used in e.g. Little Navmap for the online centers
display.

## Usage

- Download the newest airspace file
  [here](https://gitlab.com/bfaller/vatsim-airspace-openair/-/jobs/artifacts/master/raw/vatsim_airspaces.txt?job=build).
- To use the file with
  [Little Navmap](https://albar965.github.io/littlenavmap.html) (LNM), copy the
  file to your User Airspaces folder and load it. See the
  [LNM manual](https://www.littlenavmap.org/manuals/littlenavmap/release/2.4/en/SCENERY.html#load-scenery-library-user-airspaces)
  for more information.
