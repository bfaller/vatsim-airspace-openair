#!/bin/bash
# Dowloads FIRBoundaries.dat and VATSpy.dat
set -ex

VATSIM_API="https://api.vatsim.net/api/map_data/"

curl -s "$VATSIM_API" | jq -r '.fir_boundaries_dat_url' | xargs wget
curl -s "$VATSIM_API" | jq -r '.vatspy_dat_url' | xargs wget
